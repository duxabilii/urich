<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AlbumRequest;
use App\Models\Album;
use App\Services\AlbumService;

class AlbumController extends Controller
{
    /*
     * @var AlbumService $albumService
     */
    private $albumService;

    public function __construct(AlbumService $albumService)
    {
        $this->albumService = $albumService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albums = Album::with('latestphoto')->get();
        return view('admin.album.index', [
            'albums' => $albums
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.album.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AlbumRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AlbumRequest $request)
    {
        $album = $this->albumService->store($request);
        return redirect()->route('admin.album.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Album $album
     * @return \Illuminate\Http\Response
     */
    public function show(Album $album)
    {
        return view('admin.album.show', ['album' => $album]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Album $album
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Album $album)
    {
        return view('admin.album.edit', ['album' => $album]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AlbumRequest $request
     * @param  \App\Models\Album $album
     * @return \Illuminate\Http\Response
     */
    public function update(AlbumRequest $request, Album $album)
    {
        $this->albumService->store($request, $album);
        return redirect()->route('admin.album.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Album $album
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Album $album)
    {
        try {
            $album->delete();
        } catch (\Exception $e) {
            return redirect()->route('admin.album.index')
                             ->withErrors($e->getMessage());
        }
        return redirect()->route('admin.album.index');
    }
}
