<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderAlbumRequest;
use App\Http\Requests\OrderPhotoRequest;
use App\Models\Album;
use App\Models\Photo;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Update order of albums
     *
     * @param OrderAlbumRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function orderAlbum(OrderAlbumRequest $request)
    {
        $order        = $request->get('order');
        $album        = Album::findOrFail($request->get('album'));
        $album->order = $order;
        $album->save();
        return response()->json('success');

    }

    /**
     * Update order of photos
     *
     * @param OrderPhotoRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function orderPhoto(OrderPhotoRequest $request)
    {
        $order        = $request->get('order');
        $photo        = Photo::findOrFail($request->get('photo'));
        $photo->order = $order;
        $photo->save();
        return response()->json('success');

    }
}
