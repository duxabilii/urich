<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PhotoRequest;
use App\Models\Album;
use App\Models\Photo;
use App\Services\PhotoService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PhotoController extends Controller
{
    private $photoService;

    public function __construct(PhotoService $photoService)
    {
        $this->photoService = $photoService;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Album $album
     * @return \Illuminate\Http\Response
     */
    public function create(Album $album)
    {
        return view('admin.photo.create', [

            'album' => $album
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PhotoRequest $request
     * @param Album $album
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PhotoRequest $request, Album $album)
    {
        $this->photoService->create($request, $album);
        return redirect()->route('admin.album.show', $album);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Photo $photo
     * @return \Illuminate\Http\Response
     */
    public function show(Photo $photo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Photo $photo
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
    {
        return view('admin.photo.edit', [
            'photo' => $photo
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Photo $photo
     * @return \Illuminate\Http\Response
     */
    public function update(PhotoRequest $request, Photo $photo)
    {
        $this->photoService->update($request, $photo);
        return redirect()->route('admin.album.show', $photo->album);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Photo $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photo $photo)
    {
        try {
            $photo->delete();
        } catch (\Exception $e) {
            return redirect()->route('admin.album.show', $photo->album)
                             ->withErrors($e->getMessage());
        }
        return redirect()->route('admin.album.show', $photo->album);
    }
}
