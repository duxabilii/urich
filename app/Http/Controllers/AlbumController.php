<?php

namespace App\Http\Controllers;

use App\Models\Album;
use Illuminate\Http\Request;

class AlbumController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Album $album
     * @return \Illuminate\Http\Response
     */
    public function show(Album $album)
    {
        $photos = $album->photos()
                        ->get();
        return view('album.show', [
            'album' => $album,
            'photos' => $photos,
        ]);
    }

}
