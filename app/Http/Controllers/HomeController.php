<?php

namespace App\Http\Controllers;

use App\Models\Album;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albums = Album::with('latestphoto')
                       ->paginate(12);

        return view('home', [
            'albums' => $albums
        ]);
    }
}
