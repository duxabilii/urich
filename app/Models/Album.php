<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('order', 'asc')
                ->orderBy('created_at', 'desc');
        });

        static::deleting(function (Album $album) {
            foreach ($album->photos as $photo) {
                unlink(public_path('photos') . DIRECTORY_SEPARATOR . $photo->filename);
            }
            $album->photos()->delete();
        });
    }

    protected $fillable = [
        'title',
        'description',
        'order'
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
    ];

    protected $attributes = [
        'order' => 0
    ];

    /**
     * Album's photos
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    /**
     * Amount of photo in album
     *
     * @return int
     */
    public function getPhotoCountAttribute()
    {
        return $this->hasMany(Photo::class)->count();
    }

    /**
     * Get latest photo for album
     *
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|null|object
     */
    public function latestPhoto()
    {
        return $this->hasOne(Photo::class)
            ->orderBy('order')
            ->latest()
            ->withDefault(
                [
                    'filename' => 'noimage.png'
                ]
            );
    }
}
