<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('order', 'asc')
                    ->orderBy('created_at', 'desc');
        });

        /*
         * Delete file before before model delete
         */
        static::deleting(function (Photo $photo) {
            unlink(public_path('photos') . DIRECTORY_SEPARATOR . $photo->filename);
        });
    }

    protected $fillable = [
        'title',
        'filename',
        'order',
        'album_id'
    ];

    /**
     * Album
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function album()
    {
        return $this->belongsTo(Album::class);
    }
}
