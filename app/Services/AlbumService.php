<?php

namespace App\Services;


use App\Http\Requests\AlbumRequest;
use App\Models\Album;

class AlbumService
{
    public function store(AlbumRequest $request, Album $album = null)
    {
        if (!$album) {
            return Album::create($request->toArray());
        }
        return $album->fill($request->toArray())
            ->save();
    }
}