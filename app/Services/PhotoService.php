<?php

namespace App\Services;


use App\Http\Requests\PhotoRequest;
use App\Models\Album;
use App\Models\Photo;
use Illuminate\Http\UploadedFile;

class PhotoService
{

    public function create(PhotoRequest $request, Album $album) {
        $filename = $this->renameAndMovePhoto($request->file('filename'));
        return Photo::create(
            [
                'title' => $request->get('title'),
                'filename' => $filename,
                'album_id' => $album->id
            ]
        );
    }

    public function update(PhotoRequest $request, Photo $photo)
    {
        // Updating
        if ($request->hasFile('filename')) {
            // Remove old photo
            unlink(public_path('photos') . DIRECTORY_SEPARATOR . $photo->filename);
            $filename = $this->renameAndMovePhoto($request->file('filename'));
        }
        return $photo->fill(
            [
                'title' => $request->get('title'),
                'filename' => $filename ?? $photo->filename
            ]
        )->save();

    }

    /**
     * Generate filename for uploaded image
     *
     * @param string $extension
     * @return string
     */
    private function generateFilename(string $extension)
    {

        return md5(str_random(15)) . '.' . $extension;
    }

    /**
     * Move and rename uploaded image
     *
     * @param UploadedFile $file
     * @return string
     */
    private function renameAndMovePhoto(UploadedFile $file)
    {
        $extension = $file->getClientOriginalExtension();
        $filename  = $this->generateFilename($extension);
        $file->move(public_path('photos'), $filename);
        return $filename;
    }

}