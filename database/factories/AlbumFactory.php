<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Album::class, function (Faker $faker) {
    return [
        'title' => $faker->text(10),
        'description' => $faker->text(50),
        'order' => $faker->randomDigit,
        'created_at' => $faker->dateTimeBetween('-3 days', '+3 days'),
    ];
});
