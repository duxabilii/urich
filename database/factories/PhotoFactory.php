<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Photo::class, function (Faker $faker) {
    return [
        'title' => $faker->text(15),
        'filename' => $faker->image('public\photos', 640, 480, null, false, true),
        'order' => $faker->randomDigit,
        'created_at' => $faker->dateTimeBetween('-3 days', '+3 days'),
    ];
});
