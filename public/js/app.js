$(document).ready(function () {
    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox({
            showArrows: false
        });
    });

    $('.album-order').on('change', function (e) {
        var album = $(this).data('album');
        var order = $(this).val();
        $.ajax({
            url: '/admin/ajax/order-album',
            method: 'post',
            data: {
                "order": order,
                "album": album
            },
            dataType: 'JSON',
            success: function (json) {
                window.location.reload(true);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
    $('.photo-order').on('change', function (e) {
        var photo = $(this).data('photo');
        var order = $(this).val();
        $.ajax({
            url: '/admin/ajax/order-photo',
            method: 'post',
            data: {
                "order": order,
                "photo": photo
            },
            dataType: 'JSON',
            success: function (json) {
                window.location.reload(true);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
});