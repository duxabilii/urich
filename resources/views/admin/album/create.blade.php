@extends('layouts.app')

@section('content')

    <h1 class="py-2">
        Create album
    </h1>
    <div class="row py-2">
        <div class="col-12">
            <form method="POST" action="{{ route('admin.album.store') }}">
                <div class="form-group">
                    <label for="title">Album title</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}" />
                </div>
                <div class="form-group">
                    <label for="description">Album description</label>
                    <textarea id="description" name="description" class="form-control" rows="5">
                        {{ old('description') }}
                    </textarea>
                </div>
                @csrf
                <button type="submit" class="btn btn-primary">Create</button>
            </form>
        </div>
    </div>

@endsection
