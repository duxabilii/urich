@extends('layouts.app')

@section('content')

    <h1 class="py-2">
        Edit album
    </h1>
    <div class="row py-2">
        <div class="col-12">
            <form method="POST" action="{{ route('admin.album.update', $album) }}">
                @method('PUT')
                <div class="form-group">
                    <label for="title">Album title</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{ old('title') ?? $album->title }}" />
                </div>
                <div class="form-group">
                    <label for="description">Album description</label>
                    <textarea id="description" name="description" class="form-control" rows="5">
                        {{ old('description') ?? $album->description }}
                    </textarea>
                </div>
                @csrf
                <button type="submit" class="btn btn-primary">Edit</button>
            </form>
        </div>
    </div>

@endsection
