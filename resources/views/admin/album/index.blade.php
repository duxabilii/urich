@extends('layouts.app')

@section('content')

    <h1 class="py-2">
        Albums
    </h1>
    <div class="row py-2">
        <div class="col-12">
            <a href="{{ route('admin.album.create') }}" class="btn btn-success btn-lg">+ Add album</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 py-2 d-flex align-items-stretch">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Order</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Created At</th>
                    <th>Cover Image</th>
                    <th>Photos amount</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($albums as $album)
                    <tr>
                        <td class="align-middle text-center" style="width: 9em">
                            <div class="form-group">
                                {!! Form::select('order', range(0, $albums->count()), $album->order, ['class' => 'text-center album-order', 'data-album' => $album->id]) !!}
                            </div>
                        </td>
                        <td class="align-middle">
                            {{ $album->title }}
                        </td>
                        <td class="align-middle">
                            {{ $album->description }}
                        </td>

                        <td class="align-middle">
                            {{ $album->created_at }}
                        </td>
                        <td class="align-middle">
                            <img src="/photos/{{ $album->latestphoto->filename }}" class="img-thumbnail img-fluid"/>
                        </td>

                        <td class="align-middle text-center">
                            <span class="badge badge-primary badge-pill">{{ $album->photo_count }}</span>
                        </td>
                        <td class="align-middle text-center">
                            <a class="btn btn-success" href="{{ route('admin.album.edit', $album) }}">
                                Edit album
                            </a>
                        </td>
                        <td class="align-middle text-center">
                            <a class="btn btn-warning" href="{{ route('admin.album.show', $album) }}">
                                View/edit photos
                            </a>
                        </td>
                        <td class="align-middle text-center">
                            <a class="btn btn-danger"
                               onclick="event.preventDefault();if(confirm('Delete album???'))document.getElementById('album-destroy-{{ $album->id }}').submit();">
                                Delete album
                            </a>
                            <form action="{{ route('admin.album.destroy', $album) }}" method="post"
                                  id="album-destroy-{{ $album->id }}" class="hidden">
                                {!! method_field('delete') !!}
                                {!! csrf_field() !!}
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection