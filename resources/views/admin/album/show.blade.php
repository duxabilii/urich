@extends('layouts.app')

@section('content')

    <h1 class="py-2">
        Album's title "{{ $album->title }}"
    </h1>
    <h3>
        {{ $album->description }}
    </h3>

    <div class="row py-2">
        <div class="col-12">
            <a href="{{ route('admin.photo.create', $album) }}" class="btn btn-success btn-lg">+ Upload new photo</a>
        </div>
    </div>

    <div class="row">
        @foreach($photos = $album->photos as $photo)
            <div class="col-md-3 py-2 d-flex align-items-stretch">
                <div class="card">
                    <img class="card-img-top" src="/photos/{{ $photo->filename }}" alt="{{ $photo->title }}">
                    <div class="card-body">
                        <h5 class="card-title">{{ $photo->title }}</h5>
                    </div>
                    <div class="card-footer">
                        <p>Order:</p>
                        <div class="form-group">
                            {!! Form::select('order', range(0, $photos->count()), $photo->order, ['class' => 'text-center photo-order form-control', 'data-photo' => $photo->id]) !!}
                        </div>
                        <a class="btn btn-warning" href="{{ route('admin.photo.edit', $photo) }}">
                            Edit
                        </a>
                        <a class="btn btn-danger"
                           onclick="event.preventDefault();if(confirm('Delete photo???'))document.getElementById('photo-destroy-{{ $photo->id }}').submit();"
                           href="#">
                            Delete
                        </a>
                        <form action="{{ route('admin.photo.destroy', $photo) }}" method="post"
                              id="photo-destroy-{{ $photo->id }}" class="hidden">
                            {!! method_field('delete') !!}
                            {!! csrf_field() !!}
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
