@extends('layouts.app')

@section('content')

    <h1 class="py-2">
        Photo upload to album "{{ $album->title }}"
    </h1>
    <div class="row py-2">
        <div class="col-12">
            <form method="POST" action="{{ route('admin.photo.store', $album) }}" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="title">Photo title</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}"/>
                </div>
                <div class="form-group">
                    <label for="description">File</label>
                    <input type="file" name="filename" class="form-control-file"/>
                </div>
                @csrf
                <button type="submit" class="btn btn-primary">Create</button>
            </form>
        </div>
    </div>

@endsection
