@extends('layouts.app')

@section('content')

    <h1 class="py-2">
        Edit photo
    </h1>
    <div class="row py-2">
        <div class="col-12">
            <form method="POST" action="{{ route('admin.photo.update', $photo) }}" enctype="multipart/form-data">
                @method('PUT')
                <div class="form-group">
                    <label for="title">Photo title</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{ old('title') ?? $photo->title }}"/>
                </div>
                <div class="form-group">
                    <label for="description">File (live empty for old file)</label>
                    <input type="file" name="filename" class="form-control-file" value="{{ old('filename') }}"/>
                </div>
                @csrf
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>

@endsection
