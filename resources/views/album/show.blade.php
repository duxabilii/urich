@extends('layouts.app')

@section('content')
    <div class="row py-2">
        <h2>
            {{ $album->title }}
        </h2>
    </div>
    <div class="row py-1">
        <h5>
            Created at: {{ $album->created_at }}
        </h5>
        <div class="col-12">
            {{ $album->description }}
        </div>
    </div>
    <div class="row">
        @foreach ($photos as $photo)
            <div class="col-md-3 py-2 d-flex align-items-stretch">
                <div class="card">
                    <a href="/photos/{{ $photo->filename }}"
                       data-toggle="lightbox"
                       data-footer="{{ $photo->title }}">
                        <img class="card-img-top" src="/photos/{{ $photo->filename }}" class="img-fluid"
                             alt="{{ $photo->title }}"
                        />
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@endsection
