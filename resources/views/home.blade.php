@extends('layouts.app')

@section('content')

    <div class="row">
        @foreach ($albums as $album)
            <div class="col-md-4 py-2 d-flex align-items-stretch">
                <div class="card">

                    <a href="{{ route('album.show', $album) }}">
                        <img class="card-img-top" src="/photos/{{ $album->latestphoto->filename }}" class="img-fluid"
                             alt="{{ $album->latestphoto->title }}"/>
                    </a>

                    <div class="card-body">
                        <h5 class="card-title">
                            <a href="{{ route('album.show', $album) }}">{{ $album->title }}</a>
                        </h5>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row py-1 justify-content-center">
        {{ $albums->links() }}
    </div>
@endsection
