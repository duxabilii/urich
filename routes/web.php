<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::middleware('auth')
     ->prefix('admin')
     ->namespace('Admin')
     ->as('admin.')
     ->group(function () {

         //Route::get('/', 'AlbumController@index')->name('index');
         Route::resource('album', 'AlbumController');
         /*
          * Photo routes
          */
         Route::get('photo/create/{album}', 'PhotoController@create')
              ->name('photo.create');
         Route::post('photo/create/{album}', 'PhotoController@store')
              ->name('photo.store');
         Route::get('/photos/{photo}/edit', 'PhotoController@edit')
              ->name('photo.edit');
         Route::put('/photos/{photo}', 'PhotoController@update')
              ->name('photo.update');
         Route::delete('/photos/{photo}', 'PhotoController@destroy')
              ->name('photo.destroy');

         Route::middleware('ajax')
              ->group(function () {
                  Route::post('/ajax/order-album', 'OrderController@orderAlbum');
                  Route::post('/ajax/order-photo', 'OrderController@orderPhoto');
              });
     });

Route::get('/album/{album}', 'AlbumController@show')
     ->name('album.show');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('index');
Auth::routes();


